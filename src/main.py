from fastapi import FastAPI, status, Depends, HTTPException
from typing import List
from . import models,database,schemas
from .database import engine, SessionLocal
from sqlalchemy.orm import Session
from .database import get_db
from fastapi.encoders import jsonable_encoder

models.Base.metadata.create_all(bind=engine)

app=FastAPI()


@app.post("/addTask",status_code=status.HTTP_201_CREATED)
def createTask(request:schemas.TaskBase,db:Session=Depends(get_db)):
    new_task=models.Task(taskName=request.taskName,taskDesc=request.taskDesc)
    print(new_task)
    db.add(new_task)
    db.commit()
    db.refresh(new_task)
    return new_task

@app.get("/viewTasks",status_code=status.HTTP_200_OK,response_model=List[schemas.Task])
def viewTasks(db:Session=Depends(get_db)):
    print(type(db.query(models.Task).all()))
    return db.query(models.Task).all()

@app.get("/viewTask/{id}",status_code=status.HTTP_200_OK,response_model=schemas.Task)
def viewTasks(id:int,db:Session=Depends(get_db)):
    task = db.query(models.Task).filter(models.Task.taskId == id).first()
    if not task:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Task with id {id} not found")
    return task

@app.put("/updateTask/{id}", status_code=status.HTTP_202_ACCEPTED)
def updateTask(id:int,request:schemas.TaskBase,db:Session=Depends(get_db)):
    task=db.query(models.Task).filter(models.Task.taskId==id)
    if not task.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Task with id {id} not found")
    # print(type(request))
    # print(type(task))
    task_json=jsonable_encoder(request)
    task.update(task_json)
    db.commit()
    #db.refresh(task)
    #print(task)
    #db.refresh(task.first())
    return {"message":"Task Updated"}

@app.delete("/deleteTask/{id}", status_code=status.HTTP_204_NO_CONTENT)
def deleteTask(id:int,db:Session=Depends(get_db)):
    db.query(models.Task).filter(models.Task.taskId==id).delete(synchronize_session=False)
    db.commit()
    return {"message":"Task Deleted"}