from sqlalchemy import Column,Integer,String
from .database import Base

class Task(Base):
    __tablename__='tasks'
    taskId=Column(Integer,primary_key=True,index=True)
    taskName=Column(String,index=True)
    taskDesc=Column(String)