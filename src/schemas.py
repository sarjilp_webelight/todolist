from pydantic import BaseModel

class TaskBase(BaseModel):
    taskName:str
    taskDesc:str

class Task(TaskBase):
    class Config():
        orm_mode=True